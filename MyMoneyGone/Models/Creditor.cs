﻿using System;
using System.Collections.Generic;

namespace MyMoneyGone.Models
{
    public class Creditor
    {
        public Creditor()
        {
        }

        public int CreditorId { get; set; }
        public string CreditorName { get; set; }

        public IEnumerable<Expense> Expenses { get; set; }
        public IEnumerable<CreditorTag> CreditorTags { get; set; }

    }
}

/*TODO: 
 * Add functionality for Creditor Tags
 */