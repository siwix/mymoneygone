﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyMoneyGone.Models
{
    public class CreateExpenseViewModel
    {
        public CreateExpenseViewModel()
        {
        }

        public int ExpenseId { get; set; }

        [DataType(DataType.Date)]
        public DateTime ExpenseDate { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        [Display(Name = "Selected tags")]
        public List<string> TagIds { get; set; }

        //public IEnumerable<ExpenseTag> ExpenseTags { get; set; }
        [Display(Name ="Tags")]
        public MultiSelectList Tags { get; set; }


        public string CreditorId { get; set; }
        public SelectList CreditorList { get; set; }




    }
}
