﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MyMoneyGone.Models
{
    public class SeedExpense
    {
        public SeedExpense()
        {
        }

        public static async void Initialize(IServiceProvider serviceProvider)
        {
            using (var db = new ExpenseContext(serviceProvider.GetRequiredService<
                    DbContextOptions<ExpenseContext>>()))
            {



                db.Database.ExecuteSqlCommand("Delete from Expense");
                db.Database.ExecuteSqlCommand("Delete from Tag");
                db.Database.ExecuteSqlCommand("Delete from ExpenseTag");
                db.Database.ExecuteSqlCommand("Delete from Creditor");
                db.Database.ExecuteSqlCommand("Delete from CreditorTag");

                db.SaveChanges();

                /*Categories
                 * Utilities
                 * Council
                 * Car
                 * Insurance
                 * Groceries
                 * Finance
                 * Tax
                 * Take away
                 * Restaurants
                 * Health
                 * Leisure
                 * Sports
                 * Music
                 * Streaming
                 * Phone
                 * Internet
                 */

                var tags = new List<Tag> {
                    new Tag { TagId = 1, TagName = "Insurances" },
                    new Tag { TagId = 2, TagName = "Utilities" },
                    new Tag { TagId = 3, TagName = "Internet" },
                    new Tag { TagId = 4, TagName = "Rates" },
                    new Tag { TagId = 5, TagName = "Phones" },
                    new Tag { TagId = 6, TagName = "Leisure" },
                    new Tag { TagId = 7, TagName = "School" },
                    new Tag { TagId = 8, TagName = "Groceries" },
                    new Tag { TagId = 9, TagName = "Leisure" },
                    new Tag { TagId = 10, TagName = "Transport" },
                    new Tag { TagId = 11, TagName = "Car" },
                    new Tag { TagId = 12, TagName = "Car Insuranse",  ParentTags = new List<TagRelation> {
                        new TagRelation {TagId = 12, ParentTagId = 11 },
                        new TagRelation {TagId = 12, ParentTagId = 1 },
                        }
                        },
                };





                await db.Tag.AddRangeAsync(tags);


                //db.SaveChanges();

                var creditors = new List<Creditor>() {
                    new Creditor
                    {
                        CreditorId = 1,
                        CreditorName = "Origin",
                        CreditorTags = new List<CreditorTag> { new CreditorTag { CreditorId = 1, TagId = 2 } }
                    },
                    new Creditor
                    {
                        CreditorId = 2,
                        CreditorName = "Belong",
                        CreditorTags = new List<CreditorTag> { new CreditorTag { CreditorId = 2, TagId = 5 } }
                    },
                    new Creditor
                    {
                        CreditorId = 3,
                        CreditorName = "The Gardens",
                        CreditorTags = new List<CreditorTag> { new CreditorTag { CreditorId = 3, TagId = 4 } }
                    },
                    new Creditor
                    {
                        CreditorId = 4,
                        CreditorName = "Yarra Council",
                        CreditorTags = new List<CreditorTag> { new CreditorTag { CreditorId = 4, TagId = 4 } }
                    },
                    new Creditor
                    {
                        CreditorId = 5,
                        CreditorName = "Woolworths",
                        CreditorTags = new List<CreditorTag> { new CreditorTag { CreditorId = 5, TagId = 8 } }
                    }
                };

                await db.Creditor.AddRangeAsync(creditors);


                var expenses = new List<Expense>() {
                    new Expense
                    {
                        ExpenseId = 1,
                        ExpenseDate = new DateTime(2019, 02, 02),
                        Creditor = creditors[0],
                        Description = "Gas Bill",
                        ExpenseTags = new List<ExpenseTag> { new ExpenseTag { TagId = 2, ExpenseId = 1} },
                        Amount = 10.0m
                    }, new Expense
                    {
                        ExpenseId = 2,
                        ExpenseDate = new DateTime(2019, 02, 10),
                        Creditor = creditors[0],
                        Description = "Electricity Bill",
                        ExpenseTags = new List<ExpenseTag>{ new ExpenseTag { TagId = 2, ExpenseId = 2 }},
                        Amount = 20.0m
                    }, new Expense
                    {
                        ExpenseId = 3,
                        ExpenseDate = new DateTime(2019, 02, 15),
                        Creditor = creditors[2],
                        Description = "Owners Corporation Bill",
                        ExpenseTags = new List<ExpenseTag> {new ExpenseTag { TagId = 3, ExpenseId = 3 } },
                        Amount = 20.0m
                    }, new Expense
                    {
                        ExpenseId = 4,
                        ExpenseDate = new DateTime(2019, 01, 02),
                        Creditor = creditors[1],
                        Description = "Mobile Phone Bill",
                        ExpenseTags = new List<ExpenseTag>() {new ExpenseTag { TagId = 5, ExpenseId = 4 } },
                        Amount = 10.0m
                    }, new Expense
                    {
                        ExpenseId = 5,
                        ExpenseDate = new DateTime(2019, 02, 15),
                        Creditor = creditors[1],
                        Description = "Petrol",
                        ExpenseTags = new List<ExpenseTag>() {new ExpenseTag { TagId = 11, ExpenseId = 5 } },
                        Amount = 80.0m
                    }
                };


                await db.Expense.AddRangeAsync(expenses);

                await db.SaveChangesAsync();

            }
        }


    }
}
