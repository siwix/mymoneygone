﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMoneyGone.Models
{
    public class ExpenseTag
    {
        public ExpenseTag()
        {
        }

        [ForeignKey("Expense")]
        public int ExpenseId { get; set; }
        public Expense Expense { get; set; }
        [ForeignKey("Tag")]
        public int TagId { get; set; }
        public Tag Tag { get; set; }

    }


}
