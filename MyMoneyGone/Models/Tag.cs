﻿using System;
using System.Collections.Generic;

namespace MyMoneyGone.Models
{
    public class Tag
    {
        public Tag()
        {
        }


        public int TagId { get; set; }
        public string TagName { get; set; }

        public IEnumerable<TagRelation> ParentTags { get; set; } //Self join to several parent tags
        public IEnumerable<TagRelation> ChildTags { get; set; } //Self join to several chilr tags

        public IEnumerable<ExpenseTag> ExpenseTags { get; set; } //Many to many join with Expense
        public IEnumerable<CreditorTag> CreditorTags { get; set; } //Many to many join with Creditors
    }
}
