﻿using System;
using System.Collections.Generic;

namespace MyMoneyGone.Models
{
    public class TagRelation
    {
        public TagRelation()
        {
        }

        public int ParentTagId { get; set; }
        public int TagId { get; set; }

        public virtual Tag ParentTag { get;set; }
        public virtual Tag Tag { get; set; }
    }
}
