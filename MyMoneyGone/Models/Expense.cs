﻿//dotnet aspnet-codegenerator controller -name ExpensesController -m Expense -dc ExpenseContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyMoneyGone.Models
{
    public class Expense
    {
        public Expense()
        {
        }

        public int ExpenseId { get; set; }

        [DataType(DataType.Date)]
        public DateTime ExpenseDate { get; set; }

        public int CreditorId { get; set; }
        public Creditor Creditor { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        public IEnumerable<ExpenseTag> ExpenseTags { get; set; }

    }
}
