﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyMoneyGone.Models
{
    public class CreditorTag
    {
        public CreditorTag()
        {
        }

        [ForeignKey("Creditor")]
        public int CreditorId { get; set; }
        public Creditor Creditor { get; set; }
        [ForeignKey("Tag")]
        public int TagId { get; set; }
        public Tag Tag { get; set; }

    }


}
