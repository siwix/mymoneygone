using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMoneyGone.Models;

namespace MyMoneyGone.Controllers
{
    public class ExpensesController : Controller
    {
        private readonly ExpenseContext _context;

        public ExpensesController(ExpenseContext context)
        {
            _context = context;
        }

        // GET: Expenses
        public async Task<IActionResult> Index()
        {
            var tag = await _context.Tag.Include(t=>t.ParentTags).ToListAsync();



            var data = await _context.Expense.Include(cr=>cr.Creditor).Include(et => et.ExpenseTags).ThenInclude(t=>t.Tag).ToListAsync();

            return View(data);
        }

        // GET: Expenses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense
                .FirstOrDefaultAsync(m => m.ExpenseId == id);
            if (expense == null)
            {
                return NotFound();
            }

            return View(expense);
        }

        // GET: Expenses/Create
        public async Task<IActionResult> Create()
        {
            var model = new CreateExpenseViewModel { ExpenseDate = DateTime.Now, CreditorId=null, TagIds = null, Amount = 0, Description = "", Tags = null, ExpenseId = 0 };
            //var model = new CreateExpenseViewModel();

            var tags = await _context.Tag.ToListAsync();

            var tagList = tags.Select(d => new SelectListItem
            {
                Text = d.TagName,
                Value = d.TagId.ToString()
            });

            ViewBag.Tags = tagList;


            var creditors = await _context.Creditor.ToListAsync();

            var creditorList = creditors.Select(d => new SelectListItem
            {
                Text = d.CreditorName,
                Value = d.CreditorId.ToString()
            });

            //ViewBag.Creditors = creditorList;
            model.CreditorList = new SelectList(creditorList, "Value", "Text");

            return View(model);
        }

        // POST: Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ExpenseId,ExpenseDate,CreditorId,Description,TagIds,Amount")] CreateExpenseViewModel createExpenseViewModel)
        {
          
            var expense = new Expense
            {
                ExpenseId = createExpenseViewModel.ExpenseId,
                CreditorId = int.Parse(createExpenseViewModel.CreditorId),
                Amount = createExpenseViewModel.Amount,
                Description = createExpenseViewModel.Description,
                ExpenseDate = createExpenseViewModel.ExpenseDate
            };

            if (ModelState.IsValid)
            {
                _context.Add(expense);
                await _context.SaveChangesAsync();

                //Then save ExpenseTags
                var expenseTags = new List<ExpenseTag>();

                foreach(var tagId in createExpenseViewModel.TagIds)
                {
                    expenseTags.Add(new ExpenseTag { ExpenseId = expense.ExpenseId, TagId = int.Parse(tagId) });
                }

                expense.ExpenseTags = expenseTags;


                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }



            return View(expense);
        }

        // GET: Expenses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense.FindAsync(id);
            if (expense == null)
            {
                return NotFound();
            }
            return View(expense);
        }

        // POST: Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ExpenseId,ExpenseDate,Description,Amount")] Expense expense)
        {
            if (id != expense.ExpenseId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expense);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpenseExists(expense.ExpenseId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expense);
        }

        // GET: Expenses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expense = await _context.Expense
                .FirstOrDefaultAsync(m => m.ExpenseId == id);
            if (expense == null)
            {
                return NotFound();
            }

            return View(expense);
        }

        // POST: Expenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var expense = await _context.Expense.FindAsync(id);
            _context.Expense.Remove(expense);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpenseExists(int id)
        {
            return _context.Expense.Any(e => e.ExpenseId == id);
        }
    }
}
