using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyMoneyGone.Models;

public class ExpenseContext : DbContext
{
    public ExpenseContext(DbContextOptions<ExpenseContext> options)
        : base(options)
    {
    }

    public DbSet<MyMoneyGone.Models.Expense> Expense { get; set; }
    public DbSet<MyMoneyGone.Models.Creditor> Creditor { get; set; }

    public DbSet<MyMoneyGone.Models.Tag> Tag { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        modelBuilder.Entity<TagRelation>()
            .HasKey(e => new { e.ParentTagId, e.TagId });

        modelBuilder.Entity<TagRelation>()
            .HasOne(e => e.Tag)
            .WithMany(e => e.ParentTags)
            .HasForeignKey(e => e.TagId);

        modelBuilder.Entity<TagRelation>()
            .HasOne(e => e.ParentTag)
            .WithMany(e => e.ChildTags)
            .HasForeignKey(e => e.TagId);


        modelBuilder.Entity<ExpenseTag>()
            .HasKey(et => new { et.ExpenseId, et.TagId });
        modelBuilder.Entity<ExpenseTag>()
            .HasOne(et => et.Expense)
            .WithMany(ex => ex.ExpenseTags)
            .HasForeignKey(et => et.ExpenseId);
        modelBuilder.Entity<ExpenseTag>()
            .HasOne(et => et.Tag)
            .WithMany(t => t.ExpenseTags)
            .HasForeignKey(et => et.TagId);

        modelBuilder.Entity<CreditorTag>()
            .HasKey(et => new { et.CreditorId, et.TagId });
        modelBuilder.Entity<CreditorTag>()
            .HasOne(et => et.Creditor)
            .WithMany(ex => ex.CreditorTags)
            .HasForeignKey(et => et.CreditorId);
        modelBuilder.Entity<CreditorTag>()
            .HasOne(et => et.Tag)
            .WithMany(t => t.CreditorTags)
            .HasForeignKey(et => et.TagId);
    }
}
